#!/bin/bash

# source https://plusbryan.com/my-first-5-minutes-on-a-server-or-essential-security-for-linux-servers

USER=
SERVER_NAME=rebel
SERVER_DOMAIN=iero.org

# Change root pwd
# passwd

# Update server name
sed -i 's/preserve_hostname: false/preserve_hostname: true/g' /etc/cloud/cloud.cfg 
echo 'manage_etc_hosts: false' >> /etc/cloud/cloud.cfg 
old_host=$(cat /etc/hostname)
echo ${SERVER_NAME}.${SERVER_DOMAIN} > /etc/hostname

sed -i s/${old_host}.ovh.net/${SERVER_NAME}.${SERVER_DOMAIN}/g /etc/hosts
sed -i s/${old_host}/${SERVER_NAME}/g /etc/hosts

apt-get update
apt-get upgrade -y

apt-get install -y fail2ban

useradd --shell /bin/bash ${USER}
mkdir /home/${USER}
mkdir /home/${USER}/.ssh
chmod 700 /home/${USER}/.ssh

echo MY_KEY > /home/${USER}/.ssh/authorized_keys

chmod 400 /home/${USER}/.ssh/authorized_keys
chown ${USER}:${USER} /home/${USER} -R

# Change user pwd
# passwd ${USER}

usermod -aG sudo ${USER}

sed -i 's/PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
sed -i 's/PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config

# Firewall
# ufw allow from {your-ip} to any port 22
ufw allow 80
ufw allow 443
ufw allow 
ufw enable

# autoupdates
apt install -y unattended-upgrades

echo 'APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Download-Upgradeable-Packages "1";
APT::Periodic::AutocleanInterval "7";
APT::Periodic::Unattended-Upgrade "1";' > /etc/apt/apt.conf.d/10periodic

#apt install -y logwatch
#echo '/usr/sbin/logwatch --output mail --mailto greg@iero.org --detail high' >> /etc/cron.daily/00logwatch

apt install -y mosh

# Install Postgresql
apt install -y postgresql postgresql-contrib

reboot