# Federated

The goal is to create a VM with 3 dockers components:
- Traefik - https://rebel.iero.org
- Pixelfed - https://pixel.iero.org
- Mastodon - https://masto.iero.org

For Traefik, the configuration is [based on this page](https://jonnev.se/traefik-with-docker-and-lets-encrypt/)
For Pixelfed, it's [from this page](https://jonnev.se/pixelfed-beta-with-docker-and-traefik/)