# Build Mastodon image
sudo mkdir /opt/mastodon_source
sudo chown iero:docker mastodon_source
git clone https://github.com/tootsuite/mastodon.git mastodon_source
cd mastodon_source
# Checkout to the latest stable branch
git checkout $(git tag -l | grep -v 'rc[0-9]*$' | sort -V | tail -n 1)
docker build -t mastodon:v2.9.3 .

sudo mkdir -p /opt/mastodon
sudo chown iero:docker /opt/mastodon
cd /opt/mastodon

# Create network
docker network create mastodon

# Create database
docker exec -i -t mastodon_db_1  /bin/bash
su - postgres
createuser -P mastodon
createdb mastodon -O mastodon

docker-compose build
docker-compose up -d
docker-compose run --rm web rails db:migrate
docker-compose run --rm web rails assets:precompile

docker-compose down
docker-compose up -d
