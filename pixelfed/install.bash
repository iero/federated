# Pixelfed https://jonnev.se/pixelfed-beta-with-docker-and-traefik/
sudo mkdir -p /opt/pixelfed_source
sudo chown $USER:docker /opt/pixelfed_source

# Check last release https://github.com/pixelfed/pixelfed/releases
git clone https://github.com/pixelfed/pixelfed.git /opt/pixelfed_source
cd /opt/pixelfed_source
git checkout v0.10.0
docker build -t pixelfed:v0.10.0 . 

sudo mkdir -p /opt/pixelfed
sudo chown $USER:docker /opt/pixelfed
cd /opt/pixelfed

# To federate
docker-compose exec app php artisan cache:clear
docker-compose exec app php artisan optimize:clear
docker-compose exec app php artisan optimize
